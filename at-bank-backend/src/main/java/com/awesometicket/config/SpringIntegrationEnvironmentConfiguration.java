package com.awesometicket.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.ws.server.endpoint.mapping.UriEndpointMapping;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

@Configuration
@ImportResource(locations = { "classpath:META-INF/spring/environment-springintegration.xml", "classpath:META-INF/spring/bank-springintegration.xml" })
public class SpringIntegrationEnvironmentConfiguration {

	@Bean(name = "commonWebServiceHttpSender")
	public HttpComponentsMessageSender getCommonWebServiceHttpSender() {
		final HttpComponentsMessageSender bean = new HttpComponentsMessageSender();
		bean.setMaxTotalConnections(10);
		bean.setConnectionTimeout(100000);
		bean.setReadTimeout(100000);
		return bean;
	}

	@Bean
	public UriEndpointMapping getTicketServiceEndpointMapping(ApplicationContext context) {
		Object bean = context.getBean("bankRepositoryService.Gateway");

		UriEndpointMapping endpointMapping = new UriEndpointMapping();
		endpointMapping.setDefaultEndpoint(bean);
		return endpointMapping;
	}
}
