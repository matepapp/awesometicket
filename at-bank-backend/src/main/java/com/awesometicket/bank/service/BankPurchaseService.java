package com.awesometicket.bank.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.xml.source.StringSourceFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class BankPurchaseService extends AbstractServiceResponder {

	public Source bankPurchaseResponse(DOMSource request) {
		if (request == null || request.getNode() == null || request.getNode().getChildNodes() == null || request.getNode().getChildNodes().getLength() < 1) {
			// TODO: send back proper error
			throw new IllegalArgumentException();
		}
		
		BigDecimal amount = getPurchaseAmount(request);
		BigDecimal newBalance = userBalance.subtract(amount);
		
		try {
			if (newBalance.compareTo(BigDecimal.ZERO) == -1) {
				String changedBalance = changeElementValue(readFile(BUY_TICKET_FAIL_FILE_LOCATION), userBalance, "NewBalance");
				return new StringSourceFactory().createSource(changedBalance);
			} else {
				userBalance = newBalance;
				String changedBalance = changeElementValue(readFile(BUY_TICKET_SUCCESS_FILE_LOCATION), userBalance, "NewBalance");
				return new StringSourceFactory().createSource(changedBalance);
			}
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			// TODO: send back error
			throw new IllegalArgumentException();
		}
	}

	private BigDecimal getPurchaseAmount(DOMSource request) {
		NodeList childNodes = request.getNode().getChildNodes();
		BigDecimal amount = BigDecimal.ZERO;
		
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if ("Amount".equalsIgnoreCase(node.getLocalName()) && node.getTextContent() != null) {
				amount = new BigDecimal(node.getTextContent());
			}
		}
		
		return amount;
	}
}
