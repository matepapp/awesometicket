package com.awesometicket.bank.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class AbstractServiceResponder {

	protected static final String DEFAULT_BANK_DETAIL_FILE_LOCATION = "/com/awesometicket/response/DefaultBankDetail.xml";
	protected static final String BUY_TICKET_SUCCESS_FILE_LOCATION = "/com/awesometicket/response/BuyTicket_Success.xml";
	protected static final String BUY_TICKET_FAIL_FILE_LOCATION = "/com/awesometicket/response/BuyTicket_Fail.xml";
	
	protected static BigDecimal userBalance = new BigDecimal(4000);
	
	protected String readFile(String path) throws IOException, URISyntaxException {
		return readFile(path, StandardCharsets.UTF_8);
	}
	
	protected String readFile(String path, Charset encoding) throws IOException, URISyntaxException {
		byte[] encoded = Files.readAllBytes(Paths.get(AbstractServiceResponder.class.getResource(path).toURI()));
		return new String(encoded, encoding);
	}
	
	protected String changeElementValue(String readFile, Object value, String elementName) {
		return readFile.replaceAll("(<" + elementName + ">)(.*)(<\\/" + elementName + ">)", "$1" + value + "$3");
	}
	
	protected String changeElementValue(String readFile, Object value) {
		return changeElementValue(readFile, value, "Balance");
	}
}
