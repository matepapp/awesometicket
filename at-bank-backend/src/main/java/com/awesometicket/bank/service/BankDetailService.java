package com.awesometicket.bank.service;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.xml.source.StringSourceFactory;

public class BankDetailService extends AbstractServiceResponder {

	public Source bankDetailResponse(DOMSource request) {
		if (request != null) {
			try {
				String fileContent = changeElementValue(readFile(DEFAULT_BANK_DETAIL_FILE_LOCATION), userBalance);
				
				return new StringSourceFactory().createSource(fileContent);
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
				// TODO: send back error
				throw new IllegalArgumentException();
			}
		} else {
			// TODO: send back error
			throw new IllegalArgumentException();
		}
	}
}
