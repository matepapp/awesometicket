package com.awesometicket.bank.service;

import java.math.BigDecimal;

import com.awesometicket.bank.service.bean.BankingDetailReply;
import com.awesometicket.bank.service.bean.BuyTicketReply;

/**
 * Service to handle banking related functions.
 * 
 * @author matepapp
 *
 */
public interface BankService {

	/**
	 * Give back more detail about the selected user's banking data.
	 * 
	 * @param userId selected user's id
	 * @return <b>BankingDetailReply</b> Contains the detail of the bank information to the actual customer.
	 */
	BankingDetailReply getBankingDetail(Integer userId);
	
	/**
	 * Buy ticket via banking service.
	 * 
	 * @param userId selected user's id
	 * @param amount the amount which need to be paid
	 * @return <b>BuyTicketReply</b> Contains the new balance after the buy. The balance will be untouched if there is error.
	 */
	BuyTicketReply buyTicket(Integer userId, BigDecimal amount);
}
