package com.awesometicket.bank.service;

import java.math.BigDecimal;

import com.awesometicket.bank.dao.BankDao;
import com.awesometicket.bank.service.bean.BankingDetailReply;
import com.awesometicket.bank.service.bean.BuyTicketReply;

public class BankServiceImpl implements BankService {

	private BankDao bankDao;
	
	public BankServiceImpl(BankDao bankDao) {
		this.bankDao = bankDao;
	}
	
	@Override
	public BankingDetailReply getBankingDetail(Integer userId) {
		return bankDao.getBankingDetail(userId);
	}

	@Override
	public BuyTicketReply buyTicket(Integer userId, BigDecimal amount) {
		return bankDao.buyTicket(userId, amount);
	}
}
