package com.awesometicket.bank.dao;

import java.math.BigDecimal;

import com.awesometicket.bank.service.bean.BankingDetailReply;
import com.awesometicket.bank.service.bean.BankingDetailRequest;
import com.awesometicket.bank.service.bean.BuyTicketReply;
import com.awesometicket.bank.service.bean.BuyTicketRequest;

public class BankDaoImpl implements BankDao {

	private BankServiceRepository bankServiceRepository;
	
	public BankDaoImpl(BankServiceRepository bankServiceRepository) {
		this.bankServiceRepository = bankServiceRepository;
	}
	
	@Override
	public BankingDetailReply getBankingDetail(Integer userId) {
		BankingDetailRequest request = new BankingDetailRequest();
		request.setUserId(userId);
		
		return bankServiceRepository.getBankingDetail(request);
	}

	@Override
	public BuyTicketReply buyTicket(Integer userId, BigDecimal amount) {
		BuyTicketRequest request = new BuyTicketRequest();
		request.setUserId(userId);
		request.setAmount(amount);
		
		return bankServiceRepository.buyTicket(request);
	}

}
