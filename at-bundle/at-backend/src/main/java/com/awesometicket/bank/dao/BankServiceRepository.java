package com.awesometicket.bank.dao;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import com.awesometicket.bank.service.bean.BankingDetailReply;
import com.awesometicket.bank.service.bean.BankingDetailRequest;
import com.awesometicket.bank.service.bean.BuyTicketReply;
import com.awesometicket.bank.service.bean.BuyTicketRequest;

/**
 * Integration end point for bank related processes.
 * 
 * @author matepapp
 *
 */
@MessagingGateway
public interface BankServiceRepository {

	/**
	 * Give back more detail about the selected user's banking data.
	 * 
	 * @param request <b>BankingDetailRequest</b> Contains the userId.
	 * @return <b>BankingDetailReply</b> Contains the detail of the bank information to the actual customer.
	 */
	@Gateway(requestChannel = "bankRepositoryService.detail.request.Channel", replyChannel = "bankRepositoryService.detail.reply.Channel")
	BankingDetailReply getBankingDetail(BankingDetailRequest request);
	
	/**
	 * Buy ticket via banking service.
	 * 
	 * @param request <b>BuyTicketRequest</b> Contains the userId and the amount which need to be paid.
	 * @return <b>BuyTicketReply</b> Contains the new balance after the buy. The balance will be untouched if there is error.
	 */
	@Gateway(requestChannel = "bankRepositoryService.buy.request.Channel", replyChannel = "bankRepositoryService.buy.reply.Channel")
	BuyTicketReply buyTicket(BuyTicketRequest request);
}
