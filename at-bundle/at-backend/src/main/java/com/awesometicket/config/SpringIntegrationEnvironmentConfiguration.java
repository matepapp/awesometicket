package com.awesometicket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import com.awesometicket.integration.JNDIStringDestinationProvider;
import com.awesometicket.integration.bank.BankDetailReplyUnmarshaller;
import com.awesometicket.integration.bank.BankDetailRequestMarshaller;
import com.awesometicket.integration.bank.BuyTicketIntegrationReplyUnmarshaller;
import com.awesometicket.integration.bank.BuyTicketIntegrationRequestMarshaller;
import com.awesometicket.integration.ticket.NewsListReplyUnmarshaller;
import com.awesometicket.integration.ticket.NewsListRequestMarshaller;
import com.awesometicket.integration.ticket.ReserveTicketReplyUnmarshaller;
import com.awesometicket.integration.ticket.ReserveTicketRequestMarshaller;
import com.awesometicket.integration.ticket.TicketDetailReplyUnmarshaller;
import com.awesometicket.integration.ticket.TicketDetailRequestMarshaller;
import com.awesometicket.integration.ticket.TicketListReplyUnmarshaller;
import com.awesometicket.integration.ticket.TicketListRequestMarshaller;

/**
 * Spring configuration for integration (marshallers / unmarshallers / other integration beans)
 * 
 * @author matepapp
 *
 */
@Configuration
@ImportResource(locations = {"classpath:META-INF/spring/environment-springintegration.xml", "classpath:META-INF/spring/ticket-springintegration.xml", "classpath:META-INF/spring/bank-springintegration.xml"})
public class SpringIntegrationEnvironmentConfiguration {
	
	@Bean(name = "ticketWS.DestinationProvider")
	public JNDIStringDestinationProvider getTicketWSDestinationProvider() {
		final JNDIStringDestinationProvider bean = new JNDIStringDestinationProvider();
		bean.setName("integration/endpoint/ticket");
		bean.setDefaultValue("http://localhost:8880/at-ticket-backend"); // TODO: refactor to a central location
		return bean;
	}
	
	@Bean(name = "bankWS.DestinationProvider")
	public JNDIStringDestinationProvider getBankWSDestinationProvider() {
		final JNDIStringDestinationProvider bean = new JNDIStringDestinationProvider();
		bean.setName("integration/endpoint/bank");
		bean.setDefaultValue("http://localhost:8881/at-bank-backend"); // TODO: refactor to a central location
		return bean;
	}
	
	@Bean(name = "commonWebServiceHttpSender")
	public HttpComponentsMessageSender getCommonWebServiceHttpSender() {
		final HttpComponentsMessageSender bean = new HttpComponentsMessageSender();
		bean.setMaxTotalConnections(10);
		bean.setConnectionTimeout(100000);
		bean.setReadTimeout(100000);
		return bean;
	}
	
	@Bean(name = "ticketList.Marshaller")
	public TicketListRequestMarshaller getTicketListRequestMarshaller() {
		return new TicketListRequestMarshaller();
	}
	
	@Bean(name = "ticketList.Unmarshaller")
	public TicketListReplyUnmarshaller getTicketListReplyUnmarshaller() {
		return new TicketListReplyUnmarshaller();
	}
	
	@Bean(name = "ticketDetail.Marshaller")
	public TicketDetailRequestMarshaller getTicketDetailRequestMarshaller() {
		return new TicketDetailRequestMarshaller();
	}
	
	@Bean(name = "ticketDetail.Unmarshaller")
	public TicketDetailReplyUnmarshaller getTicketDetailReplyUnmarshaller() {
		return new TicketDetailReplyUnmarshaller();
	}
	
	@Bean(name = "newsList.Marshaller")
	public NewsListRequestMarshaller getNewsListRequestMarshaller() {
		return new NewsListRequestMarshaller();
	}
	
	@Bean(name = "newsList.Unmarshaller")
	public NewsListReplyUnmarshaller getNewsListReplyUnmarshaller() {
		return new NewsListReplyUnmarshaller();
	}
	
	@Bean(name = "reserveTicket.Marshaller")
	public ReserveTicketRequestMarshaller getReserveTicketRequestMarshaller() {
		return new ReserveTicketRequestMarshaller();
	}
	
	@Bean(name = "reserveTicket.Unmarshaller")
	public ReserveTicketReplyUnmarshaller getReserveTicketReplyUnmarshaller() {
		return new ReserveTicketReplyUnmarshaller();
	}
	
	@Bean(name = "bankDetail.Marshaller")
	public BankDetailRequestMarshaller getBankDetailRequestMarshaller() {
		return new BankDetailRequestMarshaller();
	}
	
	@Bean(name = "bankDetail.Unmarshaller")
	public BankDetailReplyUnmarshaller getBankDetailReplyUnmarshaller() {
		return new BankDetailReplyUnmarshaller();
	}
	
	@Bean(name = "bankBuy.Marshaller")
	public BuyTicketIntegrationRequestMarshaller getBuyTicketIntegrationRequestMarshaller() {
		return new BuyTicketIntegrationRequestMarshaller();
	}
	
	@Bean(name = "bankBuy.Unmarshaller")
	public BuyTicketIntegrationReplyUnmarshaller getBuyTicketIntegrationReplyUnmarshaller() {
		return new BuyTicketIntegrationReplyUnmarshaller();
	}
}
