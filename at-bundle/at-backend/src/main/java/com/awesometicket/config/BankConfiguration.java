package com.awesometicket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.awesometicket.bank.dao.BankDao;
import com.awesometicket.bank.dao.BankDaoImpl;
import com.awesometicket.bank.dao.BankServiceRepository;
import com.awesometicket.bank.service.BankService;
import com.awesometicket.bank.service.BankServiceImpl;

/**
 * Spring configuration for banking beans
 * 
 * @author matepapp
 *
 */
@Configuration
public class BankConfiguration {

	@Bean
	public BankDao getBankDao(BankServiceRepository bankServiceRepository) {
		return new BankDaoImpl(bankServiceRepository);
	}
	
	@Bean
	public BankService getBankService(BankServiceRepository bankServiceRepository) {
		return new BankServiceImpl(getBankDao(bankServiceRepository));
	}
}
