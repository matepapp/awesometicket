package com.awesometicket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.awesometicket.tickets.dao.TicketServiceRepository;
import com.awesometicket.tickets.dao.TicketsDao;
import com.awesometicket.tickets.dao.TicketsDaoImpl;
import com.awesometicket.tickets.service.TicketService;
import com.awesometicket.tickets.service.TicketsServiceImpl;

/**
 * Spring configuration for ticket beans
 * 
 * @author matepapp
 *
 */
@Configuration
public class TicketsConfiguration {

	@Bean
	public TicketsDao getTicketsDao(TicketServiceRepository ticketServiceRepository) {
		return new TicketsDaoImpl(ticketServiceRepository);
	}
	
	@Bean
	public TicketService getTicketsService(TicketServiceRepository ticketServiceRepository) {
		return new TicketsServiceImpl(getTicketsDao(ticketServiceRepository));
	}
}
