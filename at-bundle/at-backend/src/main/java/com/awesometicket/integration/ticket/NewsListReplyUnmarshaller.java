package com.awesometicket.integration.ticket;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.awesometicket.integration.base.BaseUnmarshaller;
import com.awesometicket.integration.bean.GetNewsReply;
import com.awesometicket.integration.bean.GetTicketListReply;
import com.awesometicket.integration.bean.News;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.NewsReply;

public class NewsListReplyUnmarshaller extends BaseUnmarshaller<GetNewsReply, NewsReply>{

	private static final Logger LOGGER = Logger.getLogger(NewsListReplyUnmarshaller.class);
	
	@Override
	protected NewsReply transform(GetNewsReply src) {
		NewsReply reply = new NewsReply();
		if (src.getNewsList() != null) {
			for (News news : src.getNewsList().getNews()) {
				reply.getNews().add(convertNews(news));
			}
		}
		
		return reply;
	}
	
	private com.awesometicket.service.bean.News convertNews(News srcNews) {
		com.awesometicket.service.bean.News targetNews = new com.awesometicket.service.bean.News();
		targetNews.setDate(srcNews.getDate());
		targetNews.setMessage(srcNews.getMessage());
		
		return targetNews;
	}

	@Override
	public GetNewsReply unmarshalContent(XMLStreamReader reader) {
		try {
			return IntegrationHelper.unmarshalObject(GetNewsReply.class, reader);
		} catch (JAXBException e) {
			LOGGER.error("Unmarshalling " + GetTicketListReply.class + " was unsuccessfuly", e);
			return null;
		}
	}
}
