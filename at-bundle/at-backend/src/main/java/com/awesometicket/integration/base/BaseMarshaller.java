package com.awesometicket.integration.base;

import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.XmlMappingException;

import com.awesometicket.basetypes.bean.BaseIntegrationRequest;
import com.awesometicket.integration.util.IntegrationHelper;

/**
 * Abstract marshaller for the integration marshallers to unify the necessary processes.
 * 
 * @author matepapp
 *
 * @param <S> generic type of the source object which will be marshalled
 * @param <T> generic type of the target object which will be used by the integration
 */
public abstract class BaseMarshaller<S, T extends BaseIntegrationRequest> implements Marshaller {

	@Override
	public void marshal(Object src, Result result) throws IOException, XmlMappingException {
		@SuppressWarnings("unchecked")
		T target = transform((S) src);
		IntegrationHelper.getXmlResult(target, getQualifiedName(), result, this);
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	/**
	 * Get the qualified name.<br>
	 * Child class have to implement the proper solution to get this.
	 * @see QName
	 * @return a QName, which contains the proper namespace and local name
	 */
	protected abstract QName getQualifiedName();
	
	/**
	 * Transform the request if it is neccessary.<br>
	 * For example add header content, change the values provided by the system, etc...
	 * @see BaseIntegrationRequest
	 * @param src the object which contains the request provided by the system
	 * @return a BaseIntegrationRequest, parent of the all integration request
	 */
	protected abstract T transform(S src);
	
	/**
	 * Method for marshaling fields of the actual class.<br>
	 * Child class have to implement the proper solution to get the fields name and value.<br>
	 * <b>Recommended to use IntegrationHelper.</b>
	 * 
	 * @see XMLStreamWriter
	 * @see IntegrationHelper
	 * @param prefix <b>prefix</b> for the generated XML element, like <b>ns</b> in here &lt;<b>ns</b>:fieldName&gt;fieldValue&lt;...
	 * @param writer a stream writer to create XML content from java code
	 * @param target target class to get the proper values from it
	 * @throws XMLStreamException 
	 */
	public abstract void marshalContent(String prefix, XMLStreamWriter writer, T target) throws XMLStreamException;
}
