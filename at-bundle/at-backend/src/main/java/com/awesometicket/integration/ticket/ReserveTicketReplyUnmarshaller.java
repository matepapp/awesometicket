package com.awesometicket.integration.ticket;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.awesometicket.basetypes.bean.BaseStatus;
import com.awesometicket.basetypes.bean.Message;
import com.awesometicket.basetypes.bean.MessageType;
import com.awesometicket.integration.base.BaseUnmarshaller;
import com.awesometicket.integration.bean.GetTicketListReply;
import com.awesometicket.integration.bean.ReserveTicketIntegrationReply;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.ReserveTicketReply;

public class ReserveTicketReplyUnmarshaller extends BaseUnmarshaller<ReserveTicketIntegrationReply, ReserveTicketReply>{

	private static final Logger LOGGER = Logger.getLogger(ReserveTicketReplyUnmarshaller.class);
	
	@Override
	protected ReserveTicketReply transform(ReserveTicketIntegrationReply src) {
		ReserveTicketReply reply = new ReserveTicketReply();
		
		Message message = new Message();
		if (src.getStatus() == null || src.getStatus() == BaseStatus.ERROR) {
			message.setMessage("Ticket will not be reserved, please contact XYZ!");
			message.setType(MessageType.WARNING);
		} else {
			message.setMessage("Ticket will be reserved. Thank you!");
			message.setType(MessageType.INFO);
		}
		reply.getMessages().add(message);
		
		return reply;
	}

	@Override
	public ReserveTicketIntegrationReply unmarshalContent(XMLStreamReader reader) {
		try {
			return IntegrationHelper.unmarshalObject(ReserveTicketIntegrationReply.class, reader);
		} catch (JAXBException e) {
			LOGGER.error("Unmarshalling " + GetTicketListReply.class + " was unsuccessfuly", e);
			return null;
		}
	}

}
