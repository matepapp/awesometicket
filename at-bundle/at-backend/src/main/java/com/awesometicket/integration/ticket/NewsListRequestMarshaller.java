package com.awesometicket.integration.ticket;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.awesometicket.integration.base.BaseMarshaller;
import com.awesometicket.integration.bean.GetNewsRequest;
import com.awesometicket.integration.bean.ObjectFactory;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.NewsRequest;

/**
 * News list related request marshaller.
 * 
 * @author matepapp
 *
 */
public class NewsListRequestMarshaller extends BaseMarshaller<NewsRequest, GetNewsRequest>{

	private static final QName QUALIFIED_NAME = new ObjectFactory().createGetNewsRequest(new GetNewsRequest()).getName();
	
	@Override
	protected QName getQualifiedName() {
		return QUALIFIED_NAME;
	}

	@Override
	protected GetNewsRequest transform(NewsRequest src) {
		GetNewsRequest request = new GetNewsRequest();
		request.setFromDate(src.getFromDate());
		request.setToDate(src.getToDate());
		
		return request;
	}

	@Override
	public void marshalContent(String prefix, XMLStreamWriter writer, GetNewsRequest target) throws XMLStreamException {
		// Mandatory fields
		IntegrationHelper.marshal(prefix, writer, "FromDate", target.getFromDate(), QUALIFIED_NAME.getNamespaceURI());
		
		// Optional fields
		if (target.getToDate() != null) {
			IntegrationHelper.marshal(prefix, writer, "ToDate", target.getToDate(), QUALIFIED_NAME.getNamespaceURI());
		}
	}
}
