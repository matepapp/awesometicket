package com.awesometicket.integration;

import java.net.URI;

import javax.naming.NamingException;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.jndi.JndiTemplate;
import org.springframework.ws.client.support.destination.DestinationProvider;

/**
 * Provider to resolve destinations from String.
 * 
 * @author matepapp
 *
 */
public class JNDIStringDestinationProvider implements DestinationProvider, InitializingBean {

	private String name;
	private String defaultValue;
	private URI uri;
	
	public void setName(String name) {
		this.name = name;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		JndiTemplate jndi = new JndiTemplate();
		String uriString = "";
		try {
			uriString = jndi.lookup("java:comp/env/" + name, String.class);
		} catch (NamingException e) {
			if (defaultValue != null) {
				uriString = defaultValue;
			} else {
				// TODO: throw exception
				// Failed to lookup JNDI + name + exception
			}
		}
		
		try {
			uri = new URI(uriString);
		} catch (Exception e) {
			// TODO: throw exception
			// Uri in JNDI + name + is wrong syntax
		}
	}

	@Override
	public URI getDestination() {
		return uri;
	}

}
