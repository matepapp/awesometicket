package com.awesometicket.integration.base;

import java.io.IOException;

import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;

import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.XmlMappingException;

import com.awesometicket.basetypes.bean.BaseIntegrationReply;
import com.awesometicket.integration.util.IntegrationHelper;

/**
 * Abstract unmarshaller for the integration unmarshallers to unify the necessary processes.
 * 
 * @author matepapp
 *
 * @param <S> generic type of the source object which will be used by the integration
 * @param <T> generic type of the target object which will be unmarshalled
 */
public abstract class BaseUnmarshaller<S extends BaseIntegrationReply, T> implements Unmarshaller {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public Object unmarshal(Source src) throws IOException, XmlMappingException {
		S reply = IntegrationHelper.parsingXml(src, this);
		// TODO: handling error come from integration
		
		return transform(reply);
	}
	
	/**
	 * Transform the reply.<br>
	 * 
	 * @param src the object which contains the reply came through the integration
	 * @return a BaseIntegrationReply, parent of the all integration reqply
	 */
	protected abstract T transform(S src);
	
	/**
	 * Method for unmarshaling fields of the actual class.<br>
	 * Child class have to implement the proper solution.<br>
	 * <b>Recommended to use IntegrationHelper.</b>
	 * 
	 * @param reader a stream reader to create Java content from XML
	 * @return target integration class to get the proper values from it
	 */
	public abstract S unmarshalContent(XMLStreamReader reader);
}
