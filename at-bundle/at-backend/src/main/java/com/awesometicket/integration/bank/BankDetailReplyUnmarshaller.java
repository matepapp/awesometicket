package com.awesometicket.integration.bank;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.awesometicket.bank.service.bean.BankingDetailReply;
import com.awesometicket.integration.base.BaseUnmarshaller;
import com.awesometicket.integration.bean.BankDetail;
import com.awesometicket.integration.bean.GetBankingDetailReply;
import com.awesometicket.integration.bean.GetTicketListReply;
import com.awesometicket.integration.util.IntegrationHelper;

public class BankDetailReplyUnmarshaller extends BaseUnmarshaller<GetBankingDetailReply, BankingDetailReply> {

	private static final Logger LOGGER = Logger.getLogger(BankDetailReplyUnmarshaller.class);

	@Override
	protected BankingDetailReply transform(GetBankingDetailReply src) {
		BankingDetailReply reply = new BankingDetailReply();
		reply.setBankDetail(convertBankDetail(src.getBankDetail()));
		
		return reply;
	}
	
	private com.awesometicket.bank.service.bean.BankDetail convertBankDetail(BankDetail srcBankDetail) {
		com.awesometicket.bank.service.bean.BankDetail targetBankDetail = new com.awesometicket.bank.service.bean.BankDetail();
		targetBankDetail.setBalance(srcBankDetail.getBalance());
		targetBankDetail.setUserName(srcBankDetail.getUserName());
		
		return targetBankDetail;
	}

	@Override
	public GetBankingDetailReply unmarshalContent(XMLStreamReader reader) {
		try {
			return IntegrationHelper.unmarshalObject(GetBankingDetailReply.class, reader);
		} catch (JAXBException e) {
			LOGGER.error("Unmarshalling " + GetTicketListReply.class + " was unsuccessfuly", e);
			return null;
		}
	}

}
