package com.awesometicket.integration.bank;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.awesometicket.bank.service.bean.BankingDetailRequest;
import com.awesometicket.integration.base.BaseMarshaller;
import com.awesometicket.integration.bean.GetBankingDetailRequest;
import com.awesometicket.integration.bean.ObjectFactory;
import com.awesometicket.integration.util.IntegrationHelper;

public class BankDetailRequestMarshaller extends BaseMarshaller<BankingDetailRequest, GetBankingDetailRequest>{

	private static final QName QUALIFIED_NAME = new ObjectFactory().createGetBankingDetailRequest(new GetBankingDetailRequest()).getName();

	@Override
	protected QName getQualifiedName() {
		return QUALIFIED_NAME;
	}

	@Override
	protected GetBankingDetailRequest transform(BankingDetailRequest src) {
		GetBankingDetailRequest request = new GetBankingDetailRequest();
		request.setUserId(src.getUserId());
		
		return request;
	}

	@Override
	public void marshalContent(String prefix, XMLStreamWriter writer, GetBankingDetailRequest target)
			throws XMLStreamException {
		// Mandatory fields
		IntegrationHelper.marshal(prefix, writer, "UserId", target.getUserId(), QUALIFIED_NAME.getNamespaceURI());
		
		// Optional fields
	}
}
