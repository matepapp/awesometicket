package com.awesometicket.integration.ticket;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.awesometicket.integration.base.BaseMarshaller;
import com.awesometicket.integration.bean.GetTicketDetailsRequest;
import com.awesometicket.integration.bean.ObjectFactory;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.TicketDetailsRequest;

/**
 * Ticket detail related request marshaller.
 * 
 * @author matepapp
 *
 */
public class TicketDetailRequestMarshaller extends BaseMarshaller<TicketDetailsRequest, GetTicketDetailsRequest> {

	private static final QName QUALIFIED_NAME = new ObjectFactory().createGetTicketDetailsRequest(new GetTicketDetailsRequest()).getName();
	
	@Override
	protected QName getQualifiedName() {
		return QUALIFIED_NAME;
	}

	@Override
	protected GetTicketDetailsRequest transform(TicketDetailsRequest src) {
		GetTicketDetailsRequest request = new GetTicketDetailsRequest();
		request.setTicketId(src.getTicketId());
		
		return request;
	}

	@Override
	public void marshalContent(String prefix, XMLStreamWriter writer, GetTicketDetailsRequest target)
			throws XMLStreamException {
		// Mandatory fields
		IntegrationHelper.marshal(prefix, writer, "TicketId", target.getTicketId(), QUALIFIED_NAME.getNamespaceURI());
		
		// Optional fields
	}
}
