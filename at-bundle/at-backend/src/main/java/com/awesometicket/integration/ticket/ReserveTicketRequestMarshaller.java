package com.awesometicket.integration.ticket;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.awesometicket.integration.base.BaseMarshaller;
import com.awesometicket.integration.bean.ObjectFactory;
import com.awesometicket.integration.bean.ReserveTicketIntegrationRequest;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.ReserveTicketRequest;

public class ReserveTicketRequestMarshaller extends BaseMarshaller<ReserveTicketRequest, ReserveTicketIntegrationRequest>{

	private static final QName QUALIFIED_NAME = new ObjectFactory().createReserveTicketIntegrationRequest(new ReserveTicketIntegrationRequest()).getName();
	
	@Override
	protected QName getQualifiedName() {
		return QUALIFIED_NAME;
	}

	@Override
	protected ReserveTicketIntegrationRequest transform(ReserveTicketRequest src) {
		ReserveTicketIntegrationRequest request = new ReserveTicketIntegrationRequest();
		request.setTicketId(src.getTicketId());
		
		return request;
	}

	@Override
	public void marshalContent(String prefix, XMLStreamWriter writer, ReserveTicketIntegrationRequest target)
			throws XMLStreamException {
		// Mandatory fields
		IntegrationHelper.marshal(prefix, writer, "TicketId", target.getTicketId(), QUALIFIED_NAME.getNamespaceURI());
		
		// Optional fields
	}

}
