package com.awesometicket.integration.bank;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.awesometicket.bank.service.bean.BuyTicketRequest;
import com.awesometicket.integration.base.BaseMarshaller;
import com.awesometicket.integration.bean.BuyTicketIntegrationRequest;
import com.awesometicket.integration.bean.ObjectFactory;
import com.awesometicket.integration.util.IntegrationHelper;

public class BuyTicketIntegrationRequestMarshaller extends BaseMarshaller<BuyTicketRequest, BuyTicketIntegrationRequest> {

	private static final QName QUALIFIED_NAME = new ObjectFactory().createBuyTicketIntegrationRequest(new BuyTicketIntegrationRequest()).getName();
	
	@Override
	protected QName getQualifiedName() {
		return QUALIFIED_NAME;
	}

	@Override
	protected BuyTicketIntegrationRequest transform(BuyTicketRequest src) {
		BuyTicketIntegrationRequest request = new BuyTicketIntegrationRequest();
		request.setUserId(src.getUserId());
		request.setAmount(src.getAmount());
		
		return request;
	}

	@Override
	public void marshalContent(String prefix, XMLStreamWriter writer, BuyTicketIntegrationRequest target)
			throws XMLStreamException {
		// Mandatory fields
		IntegrationHelper.marshal(prefix, writer, "UserId", target.getUserId(), QUALIFIED_NAME.getNamespaceURI());
		IntegrationHelper.marshal(prefix, writer, "Amount", target.getAmount(), QUALIFIED_NAME.getNamespaceURI());
		
		// Optional fields
	}

}
