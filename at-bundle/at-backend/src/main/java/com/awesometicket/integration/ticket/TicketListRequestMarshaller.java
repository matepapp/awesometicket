package com.awesometicket.integration.ticket;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.awesometicket.integration.base.BaseMarshaller;
import com.awesometicket.integration.bean.GetTicketListRequest;
import com.awesometicket.integration.bean.ObjectFactory;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.TicketListRequest;

/**
 * Ticket list related request marshaller.
 * 
 * @author matepapp
 *
 */
public class TicketListRequestMarshaller extends BaseMarshaller<TicketListRequest, GetTicketListRequest> {

	private static final QName QUALIFIED_NAME = new ObjectFactory().createGetTicketListRequest(new GetTicketListRequest()).getName();
	
	@Override
	protected QName getQualifiedName() {
		return QUALIFIED_NAME;
	}

	@Override
	protected GetTicketListRequest transform(TicketListRequest src) {
		GetTicketListRequest request = new GetTicketListRequest();
		request.setUserId(src.getUserId());
		
		return request;
	}

	@Override
	public void marshalContent(String prefix, XMLStreamWriter writer, GetTicketListRequest target) throws XMLStreamException {
		// Mandatory fields
		IntegrationHelper.marshal(prefix, writer, "UserId", target.getUserId(), QUALIFIED_NAME.getNamespaceURI());
		
		// Optional fields
	}
}
