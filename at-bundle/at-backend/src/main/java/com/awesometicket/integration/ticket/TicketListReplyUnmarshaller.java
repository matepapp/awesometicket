package com.awesometicket.integration.ticket;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.awesometicket.integration.base.BaseUnmarshaller;
import com.awesometicket.integration.bean.GetTicketListReply;
import com.awesometicket.integration.bean.Ticket;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.TicketListReply;

public class TicketListReplyUnmarshaller extends BaseUnmarshaller<GetTicketListReply, TicketListReply> {

	private static final Logger LOGGER = Logger.getLogger(TicketListReplyUnmarshaller.class);
	
	@Override
	protected TicketListReply transform(GetTicketListReply src) {
		TicketListReply reply = new TicketListReply();
		if (src.getTicketList() != null) {
			for (Ticket ticket : src.getTicketList().getTicket()) {
				reply.getTicketList().add(convertTicket(ticket));
			}
		}
		
		return reply;
	}
	
	private com.awesometicket.service.bean.Ticket convertTicket(Ticket srcTicket) {
		com.awesometicket.service.bean.Ticket targetTicket = new com.awesometicket.service.bean.Ticket();
		targetTicket.setTicketId(srcTicket.getTicketId());
		targetTicket.setName(srcTicket.getName());
		targetTicket.setPrice(srcTicket.getPrice());
		targetTicket.setEventDate(srcTicket.getEventDate());
		targetTicket.setReservedDate(srcTicket.getReservedDate());
		targetTicket.setBuyDate(srcTicket.getBuyDate());
		targetTicket.setLink(srcTicket.getLink());
		targetTicket.setRemainingTicket(srcTicket.getRemainingTicket());
		targetTicket.setRating(srcTicket.getRating());
		
		return targetTicket;
	}

	@Override
	public GetTicketListReply unmarshalContent(XMLStreamReader reader) {
		try {
			return IntegrationHelper.unmarshalObject(GetTicketListReply.class, reader);
		} catch (JAXBException e) {
			LOGGER.error("Unmarshalling " + GetTicketListReply.class + " was unsuccessfuly", e);
			return null;
		}
	}
}
