package com.awesometicket.integration.util;

import java.io.IOException;
import java.io.Writer;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.transform.Source;

import org.apache.log4j.Logger;

import com.awesometicket.basetypes.bean.BaseIntegrationReply;
import com.awesometicket.basetypes.bean.BaseIntegrationRequest;
import com.awesometicket.integration.base.BaseMarshaller;
import com.awesometicket.integration.base.BaseUnmarshaller;
import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;

/**
 * Util class to help integration processes.
 * 
 * @author matepapp
 *
 */
public class IntegrationHelper {

	private static final Logger LOGGER = Logger.getLogger(IntegrationHelper.class);
	
	private static final XMLOutputFactory XML_OUT_FACTORY = new WstxOutputFactory();
	private static final XMLInputFactory XML_IN_FACTORY = new WstxInputFactory();
	
	private static final String DEFAULT_ELEMENT_PREFIX = "ns";
	private static final String XSI_PREFIX = "xsi";
	private static final String ENCODING = "UTF-8";
	private static final String XML_VERSION = "1.0";
	
	/**
	 * Create Java object from the XML received via integration.
	 * 
	 * @param source the source object which contains the XML received through the integration
	 * @param baseUnmarshaller the actual unmarshaller object for call back
	 * @return
	 */
	public static <S extends BaseIntegrationReply, T> S parsingXml(Source source, BaseUnmarshaller<S, T> baseUnmarshaller) {
		XMLStreamReader reader;
		S target;
		try {
			reader = getXmlStreamReader(source);
			try {
				while (reader.hasNext()) {
					if (reader.getEventType() == XMLStreamConstants.START_ELEMENT) {
						break;
					}
					reader.next();
				}
			} catch (XMLStreamException e) {
				// TODO: handle exception
				// Cannot parsing xml root element, cannot determine class type
			}
			
			target = baseUnmarshaller.unmarshalContent(reader);
			
			reader.close();
		} catch (Exception e) {
			LOGGER.error("Unmarshalling was unsuccessfully, for more detail: ", e);
			throw new RuntimeException("XML unmarshalling failed", e);
		}
		
		return target;
	}

	/**
	 * Create the full marshalled XML result from the Java object.
	 * 
	 * @param target the target object which field will be marshalled
	 * @param qualifiedName a full qualifiedName determined in the marshallers
	 * @param result the writer which will be filled with the XML content
	 * @param baseMarshaller the actual marshaller object for call back
	 * @throws IOException
	 */
	public static <S, T extends BaseIntegrationRequest> void getXmlResult(T target, QName qualifiedName, Result result, BaseMarshaller<S, T> baseMarshaller) throws IOException {
		XMLStreamWriter writer;
		
		try {
			writer = getXMLStreamWriter(result);
			writer = writeXMLContent(DEFAULT_ELEMENT_PREFIX, writer, qualifiedName, (T) target, baseMarshaller);
			writer.flush();
			writer.close();
		} catch (XMLStreamException e) {
			LOGGER.error("Marshalling was unsuccessfully, for more detail: ", e);
			throw new RuntimeException("XML marshalling failed", e);
		}
	}

	/**
	 * Create a new element in the XML from the given parameters.<br>
	 * The parameters come from the actual object which need to be marshalled.
	 * 
	 * @param defaultElementPrefix the element's prefix, by default it will be <b>ns</b>
	 * @param writer a stream writer to create XML content from java code
	 * @param qualifiedName a full qualifiedName determined in the marshallers
	 * @param target the target object which field will be marshalled
	 * @param baseMarshaller the actual marshaller object for call back
	 * @return the stream writer, which will contains the new element
	 * @throws XMLStreamException
	 */
	private static <S, T extends BaseIntegrationRequest> XMLStreamWriter writeXMLContent(String defaultElementPrefix, XMLStreamWriter writer, QName qualifiedName, T target, BaseMarshaller<S, T> baseMarshaller) throws XMLStreamException {
		writer = writerDefaultHeader(writer, qualifiedName);
		// Content (child will handle it)
		baseMarshaller.marshalContent(DEFAULT_ELEMENT_PREFIX, writer, (T) target);
		// Root element end
		writer.writeEndElement();
		// Document end
		writer.writeEndDocument();
		
		return writer;
	}

	/**
	 * Write a default header.<br>
	 * It will insert the default namespaces, prefix into the XML file which will be send via integration.
	 * 
	 * @param writer a stream writer to create XML content from java code
	 * @param qualifiedName a full qualifiedName determined in the marshallers
	 * @return the stream writer, which will contains the default header part
	 * @throws XMLStreamException
	 */
	private static XMLStreamWriter writerDefaultHeader(XMLStreamWriter writer, QName qualifiedName) throws XMLStreamException {
		// Document start
		writer.writeStartDocument(ENCODING, XML_VERSION);
		
		// Root element start
		writer.writeStartElement(DEFAULT_ELEMENT_PREFIX, qualifiedName.getLocalPart(), qualifiedName.getNamespaceURI());
		writer.writeNamespace(DEFAULT_ELEMENT_PREFIX, qualifiedName.getNamespaceURI());
		writer.setPrefix(XSI_PREFIX, XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
		writer.writeNamespace(XSI_PREFIX, XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
		
		return writer;
	}
	
	/**
	 * Create an XMLStreamWriter with a factory, using a Result as output.
	 * 
	 * @param result it will contains the XML writer result
	 * @return with the XMLStreamWriter 
	 */
	public static XMLStreamWriter getXMLStreamWriter(final Result result) {
		try {
			return XML_OUT_FACTORY.createXMLStreamWriter(result);
		} catch (XMLStreamException e) {
			LOGGER.error("Cannot create XML writer", e);
			throw new RuntimeException("Cannot create XML writer", e);
		}
	}
	
	/**
	 * Create an XMLStreamWriter with a factory, using a Writer as output.<br>
	 * For example if we want to redirect the result to a file, we can use FileWriter as output.
	 * 
	 * @param result it will contains the XML writer result
	 * @return with the XMLStreamWriter 
	 */
	public static XMLStreamWriter getXMLStreamWriter(final Writer writer) {
		try {
			return XML_OUT_FACTORY.createXMLStreamWriter(writer);
		} catch (XMLStreamException e) {
			LOGGER.error("Cannot create XML writer", e);
			throw new RuntimeException("Cannot create XML writer", e);
		}
	}
	
	/**
	 * Create an XMLStreamReader with a factory, using a Source as input.<br>
	 * 
	 * @param source it will contains the XML data
	 * @return with the XMLStreamReader 
	 */
	public static XMLStreamReader getXmlStreamReader(Source source) {
		try {
			return XML_IN_FACTORY.createXMLStreamReader(source);
		} catch (XMLStreamException e) {
			LOGGER.error("Cannot create XML reader", e);
			throw new RuntimeException("Cannot create XML reader", e);
		}
	}
	
	/**
	 * It will add a new element to the XML DOM<br>
	 * The given parameters will define the XML element name, value, namespace, prefix.
	 * 
	 * @param prefix <b>prefix</b> for the newly created XML element, like <b>ns</b> in here &lt;<b>ns</b>:fieldName&gt;fieldValue&lt;...
	 * @param writer a stream writer to create XML content from java code
	 * @param fieldName the newly created XML element name
	 * @param value the newly created XML element value
	 * @param nameSpace the newly created XML element namespace
	 * @return with the writer to later processes
	 * @throws XMLStreamException
	 */
	public static XMLStreamWriter marshal(String prefix, XMLStreamWriter writer, String fieldName, Object value, String nameSpace) throws XMLStreamException {
		if (value != null) {
			writer.writeStartElement(prefix, fieldName, nameSpace);
			writer.writeCharacters(value.toString());
			writer.writeEndElement();
		}
		
		return writer;
	}
	
	/**
	 * Helper method to unmarshall the actual content from the reader and create a Java object from it.
	 * 
	 * @param clazz class of the result object, which will be created
	 * @param reader reader a stream reader to create Java content from XML
	 * @return an object, which class will be determined from the given parameter
	 * @throws JAXBException
	 */
	public static <S> S unmarshalObject(Class<S> clazz, XMLStreamReader reader) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		JAXBElement<S> unmarshalledObject = unmarshaller.unmarshal(reader, clazz);
		S unmarshalledReply = unmarshalledObject.getValue();
		
		return unmarshalledReply;
	}
}
