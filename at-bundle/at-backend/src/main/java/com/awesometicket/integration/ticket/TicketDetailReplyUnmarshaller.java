package com.awesometicket.integration.ticket;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.awesometicket.integration.base.BaseUnmarshaller;
import com.awesometicket.integration.bean.GetTicketDetailsReply;
import com.awesometicket.integration.bean.GetTicketListReply;
import com.awesometicket.integration.util.IntegrationHelper;
import com.awesometicket.service.bean.Ticket;
import com.awesometicket.service.bean.TicketDetailsReply;

public class TicketDetailReplyUnmarshaller extends BaseUnmarshaller<GetTicketDetailsReply, TicketDetailsReply>{

	private static final Logger LOGGER = Logger.getLogger(TicketDetailReplyUnmarshaller.class);
	
	@Override
	protected TicketDetailsReply transform(GetTicketDetailsReply src) {
		TicketDetailsReply reply = new TicketDetailsReply();
		reply.setTicketDetail(createTicket(src.getTicketDetail()));
		
		return reply;
	}

	@Override
	public GetTicketDetailsReply unmarshalContent(XMLStreamReader reader) {
		try {
			return IntegrationHelper.unmarshalObject(GetTicketDetailsReply.class, reader);
		} catch (JAXBException e) {
			LOGGER.error("Unmarshalling " + GetTicketListReply.class + " was unsuccessfuly", e);
			return null;
		}
	}
	
	private Ticket createTicket(com.awesometicket.integration.bean.Ticket integrationTicket) {
		Ticket ticket = new Ticket();
		ticket.setTicketId(integrationTicket.getTicketId());
		ticket.setName(integrationTicket.getName());
		ticket.setPrice(integrationTicket.getPrice());
		ticket.setEventDate(integrationTicket.getEventDate());
		ticket.setDescription(integrationTicket.getDescription());
		ticket.setBuyDate(integrationTicket.getBuyDate());
		ticket.setReservedDate(integrationTicket.getReservedDate());
		ticket.setLink(integrationTicket.getLink());
		ticket.setRemainingTicket(integrationTicket.getRemainingTicket());
		ticket.setRating(integrationTicket.getRating());
		
		return ticket;
	}
}
