package com.awesometicket.integration.bank;

import java.util.Arrays;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.awesometicket.bank.service.bean.BuyTicketReply;
import com.awesometicket.basetypes.bean.BaseStatus;
import com.awesometicket.basetypes.bean.Message;
import com.awesometicket.basetypes.bean.MessageType;
import com.awesometicket.integration.base.BaseUnmarshaller;
import com.awesometicket.integration.bean.BuyTicketIntegrationReply;
import com.awesometicket.integration.bean.GetTicketListReply;
import com.awesometicket.integration.util.IntegrationHelper;

public class BuyTicketIntegrationReplyUnmarshaller extends BaseUnmarshaller<BuyTicketIntegrationReply, BuyTicketReply> {

	private static final Logger LOGGER = Logger.getLogger(BankDetailReplyUnmarshaller.class);
	
	@Override
	protected BuyTicketReply transform(BuyTicketIntegrationReply src) {
		BuyTicketReply reply = new BuyTicketReply();
		BaseStatus status = src.getStatus();
		Message message = new Message();
		if (BaseStatus.OK == status) {
			message.setType(MessageType.INFO);
		} else {
			message.setType(MessageType.ERROR);
		}
		message.setMessage(src.getMessage());
		reply.setMessages(Arrays.asList(message));
		reply.setNewBalance(src.getNewBalance());
		
		return reply;
	}

	@Override
	public BuyTicketIntegrationReply unmarshalContent(XMLStreamReader reader) {
		try {
			return IntegrationHelper.unmarshalObject(BuyTicketIntegrationReply.class, reader);
		} catch (JAXBException e) {
			LOGGER.error("Unmarshalling " + GetTicketListReply.class + " was unsuccessfuly", e);
			return null;
		}
	}

}
