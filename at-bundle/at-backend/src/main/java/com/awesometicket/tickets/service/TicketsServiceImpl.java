package com.awesometicket.tickets.service;

import java.util.Calendar;
import java.util.List;

import com.awesometicket.controller.bean.TicketPreloadResponse;
import com.awesometicket.service.bean.News;
import com.awesometicket.service.bean.ReserveTicketReply;
import com.awesometicket.service.bean.Ticket;
import com.awesometicket.service.bean.TicketDetailsReply;
import com.awesometicket.tickets.dao.TicketsDao;

public class TicketsServiceImpl implements TicketService {

	private TicketsDao ticketsDao;
	
	public TicketsServiceImpl(TicketsDao ticketsDao) {
		this.ticketsDao = ticketsDao;
	}
	
	@Override
	public TicketPreloadResponse getTicketList() {
		return ticketsDao.getTicketList();
	}

	@Override
	public List<Ticket> getUserTicketList(Integer userId) {
		return ticketsDao.getUserTicketList(userId);
	}

	@Override
	public List<News> getNews(Calendar fromDate, Calendar toDate) {
		return ticketsDao.getNews(fromDate, toDate);
	}

	@Override
	public TicketDetailsReply getTicketDetail(Integer eventId) {
		return ticketsDao.getTicketDetail(eventId);
	}

	@Override
	public ReserveTicketReply reserveTicket(Integer ticketId) {
		return ticketsDao.reserveTicket(ticketId);
	}
}
