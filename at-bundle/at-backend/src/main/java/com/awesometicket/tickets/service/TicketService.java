package com.awesometicket.tickets.service;

import java.util.Calendar;
import java.util.List;

import com.awesometicket.controller.bean.TicketPreloadResponse;
import com.awesometicket.service.bean.News;
import com.awesometicket.service.bean.ReserveTicketReply;
import com.awesometicket.service.bean.Ticket;
import com.awesometicket.service.bean.TicketDetailsReply;

/**
 * Service to handle ticket related functions.
 * 
 * @author matepapp
 *
 */
public interface TicketService {

	/**
	 * Give back the ticket list.
	 * 
	 * @return <b>TicketPreloadResponse</b> Contains a list of ticket.<br>
	 * An empty list if there is no tickets.
	 */
	TicketPreloadResponse getTicketList();
	
	/**
	 * Give back the ticket list for the actual user.
	 * 
	 * @param userId selected user's id
	 * @return A list of ticket.
	 */
	List<Ticket> getUserTicketList(Integer userId);
	
	/**
	 * Give back the news between the selected period.<br>
	 * If toPeriod is not defined the default value will be 7 days from the actual date.
	 * 
	 * @param fromDate start date
	 * @param toDate end date
	 * @return A list of news.
	 */
	List<News> getNews(Calendar fromDate, Calendar toDate);
	
	/**
	 * Give back more detail about the selected event / ticket.
	 * 
	 * @param eventId the actual event / ticket which detail need to be show
	 * @return <b>TicketDetailsReply</b> Contains the detail of the ticket.
	 */
	TicketDetailsReply getTicketDetail(Integer eventId);
	
	/**
	 * Reserve the selected ticket.
	 * 
	 * @param ticketId the actual ticket's id which need to be reserve
	 * @return <b>ReserveTicketReply</b> 
	 */
	ReserveTicketReply reserveTicket(Integer ticketId);
}
