package com.awesometicket.tickets.dao;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import com.awesometicket.service.bean.NewsReply;
import com.awesometicket.service.bean.NewsRequest;
import com.awesometicket.service.bean.ReserveTicketReply;
import com.awesometicket.service.bean.ReserveTicketRequest;
import com.awesometicket.service.bean.TicketDetailsReply;
import com.awesometicket.service.bean.TicketDetailsRequest;
import com.awesometicket.service.bean.TicketListReply;
import com.awesometicket.service.bean.TicketListRequest;

/**
 * Integration end point for ticket related processes.
 * 
 * @author matepapp
 *
 */
@MessagingGateway
public interface TicketServiceRepository {
	
	/**
	 * Give back the ticket list.<br>
	 * If the request contains the userId, the list will be filtered to the actual user<br>
	 * Otherwise it will be returned all of the tickets.
	 * 
	 * @param request <b>TicketListRequest</b> Contains the userId as an optional field.
	 * @return <b>TicketListReply</b> Contains the list of tickets.
	 */
	@Gateway(requestChannel = "ticketRepositoryService.list.request.Channel", replyChannel = "ticketRepositoryService.list.reply.Channel")
	TicketListReply getTicketList(TicketListRequest request);
	
	/**
	 * Give back more detail about the selected event / ticket.
	 * 
	 * @param request <b>TicketDetailsRequest</b> Contains the ticketId the actual event / ticket which detail need to be show
	 * @return <b>TicketDetailsReply</b> Contains the detail of the ticket.
	 */
	@Gateway(requestChannel = "ticketRepositoryService.detail.request.Channel", replyChannel = "ticketRepositoryService.detail.reply.Channel")
	TicketDetailsReply getTicketDetails(TicketDetailsRequest request);
	
	/**
	 * Give back the news between the selected period.
	 * 
	 * @param request <b>NewsRequest</b> Contains the periods, if toPeriod is not defined the default value will be 7 days from the actual date.
	 * @return <b>NewsReply</b> Contains a list of news.
	 */
	@Gateway(requestChannel = "ticketRepositoryService.news.request.Channel", replyChannel = "ticketRepositoryService.news.reply.Channel")
	NewsReply getNews(NewsRequest request);
	
	/**
	 * Reserve the selected ticket.
	 * 
	 * @param request <b>ReserveTicketRequest</b> Contains the actual ticket's id which need to be reserve
	 * @return <b>ReserveTicketReply</b>
	 */
	@Gateway(requestChannel = "ticketRepositoryService.reserve.request.Channel", replyChannel = "ticketRepositoryService.reserve.reply.Channel")
	ReserveTicketReply reserveTicket(ReserveTicketRequest request);
}
