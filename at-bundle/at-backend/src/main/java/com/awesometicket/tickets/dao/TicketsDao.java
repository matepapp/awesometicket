package com.awesometicket.tickets.dao;

import java.util.Calendar;
import java.util.List;

import com.awesometicket.controller.bean.TicketPreloadResponse;
import com.awesometicket.service.bean.News;
import com.awesometicket.service.bean.ReserveTicketReply;
import com.awesometicket.service.bean.Ticket;
import com.awesometicket.service.bean.TicketDetailsReply;

/**
 * DAO to handle ticket related data management process.
 * 
 * @author matepapp
 *
 */
public interface TicketsDao {

	/**
	 * <h1>---Webservice call---</h1><br>
	 * Give back the ticket list.
	 * 
	 * @return <b>TicketPreloadResponse</b> Contains a list of ticket.
	 */
	TicketPreloadResponse getTicketList();
	
	/**
	 * <h1>---Webservice call---</h1><br>
	 * Give back the ticket list for the actual user.
	 * 
	 * @param userId selected user's id
	 * @return A list of ticket.
	 */
	List<Ticket> getUserTicketList(Integer userId);
	
	/**
	 * <h1>---Webservice call---</h1><br>
	 * Give back the news between the selected period.<br>
	 * If toPeriod is not defined the default value will be 7 days from the actual date.
	 * 
	 * @param fromDate start date
	 * @param toDate end date
	 * @return A list of news.
	 */
	List<News> getNews(Calendar fromDate, Calendar toDate);
	
	/**
	 * <h1>---Webservice call---</h1><br>
	 * Give back more detail about the selected event / ticket.
	 * 
	 * @param eventId
	 * @return
	 */
	TicketDetailsReply getTicketDetail(Integer eventId);
	
	/**
	 * <h1>---Webservice call---</h1><br>
	 * Try to reserve the selected ticket via integration.
	 * 
	 * @param ticketId the actual ticket's id which need to be reserve
	 * @return <b>ReserveTicketReply</b> 
	 */
	ReserveTicketReply reserveTicket(Integer ticketId);
}
