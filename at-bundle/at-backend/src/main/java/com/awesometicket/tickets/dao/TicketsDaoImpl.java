package com.awesometicket.tickets.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.awesometicket.controller.bean.TicketPreloadResponse;
import com.awesometicket.service.bean.News;
import com.awesometicket.service.bean.NewsReply;
import com.awesometicket.service.bean.NewsRequest;
import com.awesometicket.service.bean.ReserveTicketReply;
import com.awesometicket.service.bean.ReserveTicketRequest;
import com.awesometicket.service.bean.Ticket;
import com.awesometicket.service.bean.TicketDetailsReply;
import com.awesometicket.service.bean.TicketDetailsRequest;
import com.awesometicket.service.bean.TicketListReply;
import com.awesometicket.service.bean.TicketListRequest;

public class TicketsDaoImpl implements TicketsDao {

	private TicketServiceRepository ticketServiceRepository;
	
	public TicketsDaoImpl(TicketServiceRepository ticketServiceRepository) {
		this.ticketServiceRepository = ticketServiceRepository;
	}
	
	@Override
	public TicketPreloadResponse getTicketList() {
		TicketListReply ticketListReply = ticketServiceRepository.getTicketList(new TicketListRequest());
		TicketPreloadResponse ticketPreloadResponse = new TicketPreloadResponse();
		ticketPreloadResponse.setTicketList(ticketListReply.getTicketList());
		ticketPreloadResponse.setMessages(ticketListReply.getMessages());
		
		return ticketPreloadResponse;
	}

	@Override
	public List<Ticket> getUserTicketList(Integer userId) {
		TicketListRequest getTicketListRequest = new TicketListRequest();
		getTicketListRequest.setUserId(userId);
		
		TicketListReply ticketListReply = ticketServiceRepository.getTicketList(getTicketListRequest);
		return ticketListReply == null ? new ArrayList<Ticket>() : ticketListReply.getTicketList();
	}

	@Override
	public List<News> getNews(Calendar fromDate, Calendar toDate) {
		NewsRequest request = new NewsRequest();
		request.setFromDate(fromDate);
		request.setToDate(toDate);
		
		NewsReply news = ticketServiceRepository.getNews(request);
		return news == null ? new ArrayList<News>() : news.getNews();
	}

	@Override
	public TicketDetailsReply getTicketDetail(Integer eventId) {
		TicketDetailsRequest request = new TicketDetailsRequest();
		request.setTicketId(eventId);
		
		return ticketServiceRepository.getTicketDetails(request);
	}

	@Override
	public ReserveTicketReply reserveTicket(Integer ticketId) {
		ReserveTicketRequest request = new ReserveTicketRequest();
		request.setTicketId(ticketId);
		
		return ticketServiceRepository.reserveTicket(request);
	}
}
