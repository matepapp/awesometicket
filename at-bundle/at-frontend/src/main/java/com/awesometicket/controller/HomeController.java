package com.awesometicket.controller;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.awesometicket.controller.bean.HomePreloadRequest;
import com.awesometicket.controller.bean.HomePreloadResponse;
import com.awesometicket.controller.bean.TicketPreloadResponse;
import com.awesometicket.service.bean.News;
import com.awesometicket.service.bean.Ticket;
import com.awesometicket.tickets.service.TicketService;

@Controller
@RequestMapping(value = "/home/")
public class HomeController {
	
	@Autowired
	private TicketService ticketService;
	
	@RequestMapping(value = "preload", method = RequestMethod.POST)
	@ResponseBody
	public HomePreloadResponse preload(@RequestBody HomePreloadRequest request) {
		HomePreloadResponse response = new HomePreloadResponse();
		
		List<News> news = ticketService.getNews(new GregorianCalendar(), new GregorianCalendar());
		response.setNews(news);
		
		TicketPreloadResponse ticketList = ticketService.getTicketList();
		response.setTicketList(ticketList != null ? ticketList.getTicketList() : new ArrayList<Ticket>());
		
		return response;
	}
}
