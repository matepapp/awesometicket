package com.awesometicket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.awesometicket.controller.bean.ReserveTicketRequest;
import com.awesometicket.controller.bean.ReserveTicketResponse;
import com.awesometicket.controller.bean.TicketDetailRequest;
import com.awesometicket.controller.bean.TicketDetailResponse;
import com.awesometicket.controller.bean.TicketListRequest;
import com.awesometicket.controller.bean.TicketListResponse;
import com.awesometicket.controller.bean.TicketPreloadRequest;
import com.awesometicket.controller.bean.TicketPreloadResponse;
import com.awesometicket.service.bean.ReserveTicketReply;
import com.awesometicket.service.bean.TicketDetailsReply;
import com.awesometicket.tickets.service.TicketService;

/**
 * Controller to handle ticket related requests / responses between frontend and backend.
 * 
 * @author matepapp
 *
 */
@Controller
@RequestMapping("/tickets/")
public class TicketsController {
	
	@Autowired
	private TicketService ticketService;
	
	@RequestMapping(value = "preload", method = RequestMethod.POST)
	@ResponseBody
	public TicketPreloadResponse preload(@RequestBody TicketPreloadRequest request) {
		TicketPreloadResponse response = new TicketPreloadResponse();
		response.setTicketList(ticketService.getTicketList().getTicketList());
		response.setMessages(ticketService.getTicketList().getMessages());
		
		return response;
	}
	
	@RequestMapping(value = "userTickets", method = RequestMethod.POST)
	@ResponseBody
	public TicketListResponse preload(@RequestBody TicketListRequest request) {
		TicketListResponse response = new TicketListResponse();
		response.setTicketList(ticketService.getUserTicketList(request.getUserId()));
		
		return response;
	}
	
	@RequestMapping(value = "getTicketDetail", method = RequestMethod.POST)
	@ResponseBody
	public TicketDetailResponse getTicketDetail(@RequestBody TicketDetailRequest request) {
		TicketDetailResponse response = new TicketDetailResponse();
		
		TicketDetailsReply ticketDetailReply = ticketService.getTicketDetail(request.getEventId());
		response.setTicketDetail(ticketDetailReply.getTicketDetail());
		response.setMessages(ticketDetailReply.getMessages());
		
		return response;
	}
	
	@RequestMapping(value = "reserve", method = RequestMethod.POST)
	@ResponseBody
	public ReserveTicketResponse reserveTicket(@RequestBody ReserveTicketRequest request) {
		ReserveTicketResponse response = new ReserveTicketResponse();
		
		ReserveTicketReply reserveTicketReply = ticketService.reserveTicket(request.getTicketId());
		response.setMessages(reserveTicketReply.getMessages());
		
		return response;
	}
}
