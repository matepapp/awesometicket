package com.awesometicket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.awesometicket.bank.controller.bean.BankingDetailRequest;
import com.awesometicket.bank.controller.bean.BankingDetailResponse;
import com.awesometicket.bank.controller.bean.BuyTicketRequest;
import com.awesometicket.bank.controller.bean.BuyTicketResponse;
import com.awesometicket.bank.service.BankService;
import com.awesometicket.bank.service.bean.BankingDetailReply;
import com.awesometicket.bank.service.bean.BuyTicketReply;

@Controller
@RequestMapping(value = "/bank/")
public class BankController {

	@Autowired
	private BankService bankService;
	
	@RequestMapping(value = "detail")
	@ResponseBody
	public BankingDetailResponse getBankingDetail(@RequestBody BankingDetailRequest request) {
		BankingDetailResponse response = new BankingDetailResponse();
		
		BankingDetailReply bankingDetailReply = bankService.getBankingDetail(request.getUserId());
		response.setBankDetail(bankingDetailReply.getBankDetail());
		response.setMessages(bankingDetailReply.getMessages());
		
		return response;
	}
	
	@RequestMapping(value = "buy")
	@ResponseBody
	public BuyTicketResponse buyTicket(@RequestBody BuyTicketRequest request) {
		BuyTicketResponse response = new BuyTicketResponse();
		
		BuyTicketReply buyTicketReply = bankService.buyTicket(request.getUserId(), request.getAmount());
		response.setNewBalance(buyTicketReply.getNewBalance());
		response.setMessages(buyTicketReply.getMessages());
		
		return response;
	}
}
