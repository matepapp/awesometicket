package com.awesometicket.filter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.ContentCachingRequestWrapper;

import com.awesometicket.exception.AbstractRequestException;
import com.awesometicket.exception.BadRequestException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * An interceptor to validate the request before it reach the controller layer.
 * If something is wrong it will set the response status to error and create 
 * a JsonObject as response.
 * 
 * @author matepapp
 *
 */
public class RequestValidatorInterceptor extends HandlerInterceptorAdapter {

	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final String BASE_CONTROLLER_PACAKGE = "com.awesometicket.controller.bean.";
	private static final Logger LOGGER = Logger.getLogger(RequestValidatorInterceptor.class);
	
	private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
	private ObjectMapper mapper = new ObjectMapper();

	/**
	 * Pre handling the actual request.<br>
	 * If the request is not valid the system will return error instead of handling it, for example:
	 * <ul>
	 * <li>Not contains the request type</li>
	 * <li>One of the field is invalid</li>
	 * <li>Exception thrown during the process</li>
	 * </ul>
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		ContentCachingRequestWrapper contentCachingRequestWrapper = new ContentCachingRequestWrapper((HttpServletRequest) request);
		
		try {
			validateRequest(new ByteArrayInputStream(contentCachingRequestWrapper.getContentAsByteArray()), contentCachingRequestWrapper.getRequestURI());
		} catch (AbstractRequestException e) {
			response.setContentType(JSON_CONTENT_TYPE);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			
			response.resetBuffer();
			response.getWriter().append(e.getJsonMessage());
			return false;
		}
		
		return super.preHandle(contentCachingRequestWrapper, response, handler);
	}

	private void validateRequest(InputStream stream, String requesturi) {
		String jsonBody = "";
		
		try {
			jsonBody = IOUtils.toString(stream, "UTF-8");
		} catch (IOException e) {
			LOGGER.debug("Parsing the request body failed, it is an invalid one.", e);
			throw new BadRequestException();
		}

		if (jsonBody == null || jsonBody.isEmpty()) {
			LOGGER.debug("Json body was empty / null for the following request: " + requesturi + ". Handling will be skipped.");
			return;
		}

		JSONObject jsonObject = new JSONObject(jsonBody);
		if (!jsonObject.has("requestType")) {
			LOGGER.debug("Request type is missing for the following request: " + requesturi);
			throw new BadRequestException();
		}

		String requestType = jsonObject.getString("requestType");
		Class<?> requestClass;
		try {
			if (!requestType.contains(BASE_CONTROLLER_PACAKGE)) {
				requestType = BASE_CONTROLLER_PACAKGE + requestType;
			}
			requestClass = Class.forName(requestType);
		} catch (ClassNotFoundException e) {
			LOGGER.debug("Request type is not a valid class type. Request type json: " + requestType);
			throw new BadRequestException();
		}

		Object unvalidatedRequest;
		try {
			unvalidatedRequest = mapper.readValue(jsonBody, requestClass);
		} catch (IOException e) {
			LOGGER.debug("Cannot parsing json: " + jsonBody + " to the requested class: " + requestClass.getName(), e);
			throw new BadRequestException();
		}

		List<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Object>> validationResult = validator.validate(unvalidatedRequest);
		for (ConstraintViolation<Object> constraintViolation : validationResult) {
			// TODO: add the property value too, message is not enough
			errors.add(constraintViolation.getMessage());
		}
		
		if (!errors.isEmpty()) {
			throw new BadRequestException(errors);
		}
	}
}
