package com.awesometicket.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

/**
 * Abstract request exception to handling the exception in a centralized location.<br>
 * It will contains the following information:
 * <ul>
 * <li>Success (<b>Boolean</b>) : <b>false</b> if the request handling or the validation failed, <b>true</b> otherwise</li>
 * <li>Message (<b>String</b>) : the message for the problem / exception</li>
 * <li>Data (<b>Map</b>) : a map which can contains additional information related the problem, like <b>errors</b> to error list</li>
 * </ul>
 * 
 * @author matepapp
 *
 */
public abstract class AbstractRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private JSONObject json;	
	
	protected static String success = "false";
	protected static String message = "";
	protected static Map<String, Object> data = new HashMap<String, Object>();
	
	/**
	 * Parsing the actual information to a JsonObject. This object will be the response of the error.
	 * 
	 * @return a formatted JSON as a String
	 */
	@SuppressWarnings("unchecked")
	public String getJsonMessage() {
		json = new JSONObject();
		json.put("message", message);
		json.put("success", success);
		
		JSONObject dataContentJson = new JSONObject();
		for (Entry<String, Object> entry : data.entrySet()) {
			if (entry.getValue() instanceof List<?>) {
				for (String listValue : (List<String>) entry.getValue()) {
					dataContentJson.append("errors", listValue);
				}
			} else if (entry.getValue() instanceof String) {
				dataContentJson.put(entry.getKey(), entry.getValue());
			}
		}
		json.put("data", dataContentJson);
		
		return json.toString();
	}
	
	protected void defineValues(List<String> errors) {
		if (errors != null && !errors.isEmpty()) {
			data.put("errors", errors);
		}
	};
}
