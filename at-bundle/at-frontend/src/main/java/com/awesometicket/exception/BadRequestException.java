package com.awesometicket.exception;

import java.util.List;

/**
 * Request related exception class.<br>
 * For instance request type is not defined, body is invalid, etc...
 * 
 * @author matepapp
 *
 */
public class BadRequestException extends AbstractRequestException {

private static final long serialVersionUID = 1L;
	
	private static final String REQUEST_IS_INVALID_EXCEPTION_TEXT = "Request is invalid!";

	public BadRequestException() {
		this(null, null);
	}
	
	public BadRequestException(List<String> errors) {
		this(null, errors);
	}
	
	public BadRequestException(String exceptionMessage, List<String> errors) {
		if (exceptionMessage == null || exceptionMessage.isEmpty()) {
			exceptionMessage = REQUEST_IS_INVALID_EXCEPTION_TEXT;
		}
		message = exceptionMessage;
		defineValues(errors);
	}

	@Override
	protected void defineValues(List<String> errors) {
		super.defineValues(errors);
	}
}
