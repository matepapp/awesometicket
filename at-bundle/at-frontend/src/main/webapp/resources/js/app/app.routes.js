angular.module('webApp')
.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl : 'resources/js/components/home/home.html',
        controller : 'HomeController',
        controllerAs: 'vm'
    })
    .when('/tickets', {
        templateUrl : 'resources/js/components/tickets/tickets.html',
        controller : 'TicketsController',
        controllerAs: 'vm'
    })
    .when('/tickets/detail/:eventId', {
        templateUrl : 'resources/js/components/tickets/detail/ticketDetail.html',
        controller : 'TicketDetailController',
        controllerAs: 'vm'
    })
    .when('/tickets/buy/:ticketId', {
        templateUrl : 'resources/js/components/tickets/checkout/ticketCheckOut.html',
        controller : 'TicketCheckOutController',
        controllerAs: 'vm'
    })
    .otherwise({
        redirectTo : '/404'
    });
}]);