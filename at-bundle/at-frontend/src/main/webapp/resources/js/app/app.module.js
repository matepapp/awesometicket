angular.module('webApp', [
    'ngRoute',
    'ui.bootstrap',
    'underscore',
    'ngTouch',
    'smart-table'
])
.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
}]);