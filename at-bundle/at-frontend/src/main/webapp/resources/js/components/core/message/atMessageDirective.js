angular.module('webApp')
.directive('atMessage', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'resources/js/components/core/message/messageDirective.html',
        scope: {
            messages: '<messages'
        },
        link: function (scope, element, attrs) {
        }
    }
});