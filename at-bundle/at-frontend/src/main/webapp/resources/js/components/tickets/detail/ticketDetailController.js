angular.module('webApp')
.controller('TicketDetailController', ['$routeParams', '$location', 'TicketsService',
    function($routeParams, $location, TicketsService) {
        var vm = this;
        var onInit = onInit;
        
        vm.back = backAction;
        vm.reserve = reserveAction;
        vm.eventId;
        
        vm.isLoadMaskActive = false;
        vm.ticketDetail;
        vm.messages = [];
        
        function onInit() {
            vm.isLoadMaskActive = true;
            vm.eventId = $routeParams.eventId;
            
            TicketsService.getTicketDetail(vm.eventId).then(function(result) {
                vm.ticketDetail = result.ticketDetail;
                vm.isLoadMaskActive = false;
            });
        }
        onInit();
        
        function backAction() {
            $location.path('/');
        }
        
        function reserveAction() {
            vm.isLoadMaskActive = true;
            
            TicketsService.reserveTicket(vm.eventId).then(function(result) {
                vm.messages = result.messages;
                vm.isLoadMaskActive = false;
            });
        }
}]);