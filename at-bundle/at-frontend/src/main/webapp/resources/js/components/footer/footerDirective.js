angular.module('webApp')
.directive('footer', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'resources/js/components/footer/footerDirective.html',
        controller: 'FooterController',
        controllerAs: 'vm'
    };
});