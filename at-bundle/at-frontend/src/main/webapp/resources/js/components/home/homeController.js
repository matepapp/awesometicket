angular.module('webApp')
.controller('HomeController', ['$location', 'HomeService',
    function($location, HomeService) {
        var vm = this;
        var onInit = onInit;
        
        vm.showEvent = showEvent;
        vm.news = [];
        vm.events = [];
        vm.isLoadMaskActive = false;
        
        function onInit() {
            vm.isLoadMaskActive = true;
            HomeService.preload().then(function(result) {
                vm.news = result.news;
                vm.events = result.ticketList;
                
                vm.isLoadMaskActive = false;
            });
        }
        onInit();
        
        function showEvent(eventId) {
            console.info('Selected event: ', eventId);
            $location.path('/tickets/detail/' + eventId);
        }
    }
]);