angular.module('webApp')
.directive('atLoadMask', function () {
    return {
        restrict: 'A',
        templateUrl: 'resources/js/components/core/loadmask/atLoadMaskDirective.html',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.atLoadMask, watchLoadMask);
            
            function watchLoadMask(newValue) {
                if (newValue) {
                    element.removeClass('ng-hide');
                } else {
                    element.addClass('ng-hide');
                }
            }
        }
    }
});
