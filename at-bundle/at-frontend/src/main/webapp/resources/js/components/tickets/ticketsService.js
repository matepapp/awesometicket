angular.module('webApp')
.service('TicketsService', function($http) {
    return {
        preload: preload,
        getTicketDetail: getTicketDetail,
        getUserTickets: getUserTickets,
        reserveTicket: reserveTicket
    };
    
    function preload() {
        var ticketPreloadRequest = {
            requestType: 'TicketPreloadRequest'
        }
        
        return $http.post('tickets/preload', ticketPreloadRequest).then(
            function successCallback(data) {
                return data.data;
            }, function errorCallback(data) {
                // TODO: handling error
                console.error('tickets/preload call failed');
                return data.data;
            });
    };
    
    function getTicketDetail(eventId) {
        var ticketDetailRequest = {
            requestType: 'TicketDetailRequest',
            eventId: eventId
        }
        
        return $http.post('tickets/getTicketDetail', ticketDetailRequest).then(
            function successCallback(data) {
                return data.data;
            }, function errorCallback(data) {
                // TODO: handling error
                console.error('tickets/getTicketDetail call failed');
                return data.data;
            });
    }
    
    function getUserTickets(userId) {
        var ticketListRequest = {
            requestType: 'TicketListRequest',
            userId: userId
        };
        
        return $http.post('tickets/userTickets', ticketListRequest).then(
            function successCallback(data) {
                return data.data;
            }, function errorCallback(data) {
                // TODO: handling error
                console.error('tickets/userTickets call failed');
                return data.data;
            });
    }
    
    function reserveTicket(ticketId) {
        var reserveTicketRequest = {
            requestType: 'ReserveTicketRequest',
            ticketId: ticketId
        }
        
        return $http.post('tickets/reserve', reserveTicketRequest).then(
            function successCallback(data) {
                return data.data;
            }, function errorCallback(data) {
                // TODO: handling error
                console.error('tickets/reserve call failed');
                return data.data;
            });
    }
});