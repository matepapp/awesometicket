angular.module('webApp')
.directive('header', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'resources/js/components/header/headerDirective.html',
        controller: 'HeaderController',
        controllerAs: 'vm'
    };
});