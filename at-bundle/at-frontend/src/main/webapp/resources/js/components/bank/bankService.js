angular.module('webApp')
.service('BankService', function($http) {
    return {
        getBankingDetail: getBankingDetail,
        buyTicket: buyTicket
    }
    
    function getBankingDetail(userId) {
        var bankingDetailRequest = {
            requestType: 'BankingDetailRequest',
            userId: userId
        }
        
        return $http.post('bank/detail', bankingDetailRequest).then(
            function successCallback(data) {
                return data.data;
            }, function errorCallback(data) {
                // TODO: handling error
                console.error('bank/detail call failed');
                return data.data;
            });
    }
    
    function buyTicket(userId, amount) {
        var buyTicketRequest = {
            requestType: 'BuyTicketRequest',
            userId: userId,
            amount: amount
        }
        
        return $http.post('bank/buy', buyTicketRequest).then(
            function successCallback(data) {
                return data.data;
            }, function errorCallback(data) {
                // TODO: handling error
                console.error('bank/buy call failed');
                return data.data;
            });
    }
});