angular.module('webApp')
.controller('TicketCheckOutController', ['$routeParams', '$location', 'TicketsService', 'BankService',
    function($routeParams, $location, TicketsService, BankService) {
        var vm = this;
        var onInit = onInit;
        
        vm.ticketId;
        vm.isLoadMaskActive = false;
        vm.ticketDetail;
        vm.bankingDetail;
        
        vm.back = backAction;
        vm.buy = buyAction;
        
        vm.messages = [];
        // TODO: test data
        vm.userId = 10;
        
        vm.isTicketLoadMaskActive = false;
        vm.isBankingDetailsLoadMaskActive = false;
        
        function onInit() {
            vm.isTicketLoadMaskActive = true;
            vm.isBankingDetailsLoadMaskActive = true;
            
            vm.ticketId = $routeParams.ticketId;
            
            TicketsService.getTicketDetail(vm.ticketId).then(function(result) {
                vm.ticketDetail = result.ticketDetail;
                vm.isTicketLoadMaskActive = false;
            });
            
            BankService.getBankingDetail().then(function(result) {
                vm.bankingDetail = result.bankDetail;
                vm.isBankingDetailsLoadMaskActive = false;
            });
//            vm.ticketDetail;
//            vm.bankingDetails;
//            vm.messages;
        }
        onInit();
        
        function backAction() {
            $location.path('/tickets');
        }
        
        function buyAction() {
            console.info('buyAction');
            var amount = vm.ticketDetail.price;
            
            vm.isTicketLoadMaskActive = true;
            BankService.buyTicket(vm.userId, amount).then(function(result) {
                vm.messages = result.messages;
                vm.bankingDetail.balance = result.newBalance;
                
                vm.isTicketLoadMaskActive = false;
            });
        }
}]);