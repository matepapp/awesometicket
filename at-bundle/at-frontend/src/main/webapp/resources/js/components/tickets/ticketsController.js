angular.module('webApp')
.controller('TicketsController', ['TicketsService', '$location',
    function(TicketsService, $location) {
        var vm = this;
        var onInit = onInit;
        
        vm.buyTicket = buyTicket;
        vm.tickets = [];
        vm.messages = [];
        vm.preload;
        vm.isLoadMaskActive = false;
        
        // TODO: test data
        vm.userId = 10;
        
        function onInit() {
            vm.isLoadMaskActive = true;
            TicketsService.getUserTickets(vm.userId).then(function(result) {
                vm.tickets = result.ticketList;
                
                vm.isLoadMaskActive = false;
            });
        }
        onInit();
        
        function buyTicket(selectedTicket) {
            $location.path('/tickets/buy/' + selectedTicket.ticketId);
        }
        
        // TODO: solve pagination
        // TODO: global search?
}]);