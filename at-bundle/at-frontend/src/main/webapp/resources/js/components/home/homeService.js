angular.module('webApp')
.service('HomeService', function($http) {
    return {
        preload: preload
    };
    
    function preload() {
        var homePreloadRequest = {
            requestType: 'HomePreloadRequest'
        }
        
        return $http.post('home/preload', homePreloadRequest).then(
            function successCallback(data) {
                return data.data;
            }, function errorCallback(data) {
                // TODO: handling error
                console.error('home/preload call failed');
                return data.data;
            });
    };
});