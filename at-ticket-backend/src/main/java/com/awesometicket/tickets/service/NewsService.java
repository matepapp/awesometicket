package com.awesometicket.tickets.service;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.xml.source.StringSourceFactory;

public class NewsService extends AbstractServiceResponder{

	public Source newsResponse(DOMSource request) {
		if (request != null) {
			try {
				return new StringSourceFactory().createSource(readFile(DEFAULT_NEWS_LIST_FILE_LOCATION));
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
				// TODO: send back error
				throw new IllegalArgumentException();
			}
		} else {
			// TODO: send back error
			throw new IllegalArgumentException();
		}
	}
}
