package com.awesometicket.tickets.service;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.xml.source.StringSourceFactory;

public class ReserveTicketService extends AbstractServiceResponder {

	private static boolean hasError = Boolean.FALSE;
	
	public Source reserveResponse(DOMSource request) {
		if (request != null) {
			try {
				if (hasError) {
					hasError = Boolean.FALSE;
					return new StringSourceFactory().createSource(readFile(RESERVE_TICKET_FAIL_FILE_LOCATION));
				} else {
					hasError = Boolean.TRUE;
					return new StringSourceFactory().createSource(readFile(RESERVE_TICKET_SUCCESS_FILE_LOCATION));
				}
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
				// TODO: send back error
				throw new IllegalArgumentException();
			}
		} else {
			// TODO: send back error
			throw new IllegalArgumentException();
		}
	}
}
