package com.awesometicket.tickets.service;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.xml.source.StringSourceFactory;
import org.w3c.dom.Node;

public class TicketDetailService extends AbstractServiceResponder {
	
	public Source ticketDetailResponse(DOMSource request) {
		if (request == null || request.getNode() == null || request.getNode().getChildNodes() == null || request.getNode().getChildNodes().getLength() < 1) {
			// TODO: send back proper error
			throw new IllegalArgumentException();
		}
		
		// TODO: check the case when the eventId is not null in the request.
		Node node = request.getNode().getChildNodes().item(0);
		String eventId = node.getTextContent();
		
		try {
			String readFile = readFile(getFileLocation(eventId));
			String changeElementValue = changeElementValue(readFile, ticketPriceMap.get(eventId), "Price");
			
			return new StringSourceFactory().createSource(changeElementValue);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			// TODO: send back error
			throw new IllegalArgumentException();
		}
	}
	
	private String getFileLocation(String eventId) {
		String fileName = DEFAULT_TICKET_DETAIL_FILE_LOCATION;
		
		if ("3".equalsIgnoreCase(eventId)) {
			fileName = "/com/awesometicket/response/TicketDetail_3.xml";
		}
		
		return fileName;
	}
}
