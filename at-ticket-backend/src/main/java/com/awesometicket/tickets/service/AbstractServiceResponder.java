package com.awesometicket.tickets.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractServiceResponder {

	protected static final String DEFAULT_TICKET_DETAIL_FILE_LOCATION = "/com/awesometicket/response/DefaultTicketDetail.xml";
	protected static final String DEFAULT_TICKET_LIST_FILE_LOCATION = "/com/awesometicket/response/DefaultTicketList.xml";
	protected static final String DEFAULT_NEWS_LIST_FILE_LOCATION = "/com/awesometicket/response/DefaultNewsList.xml";
	protected static final String RESERVE_TICKET_SUCCESS_FILE_LOCATION = "/com/awesometicket/response/ReserveTicket_Success.xml";
	protected static final String RESERVE_TICKET_FAIL_FILE_LOCATION = "/com/awesometicket/response/ReserveTicket_Fail.xml";
	
	protected static Map<String, Integer> ticketPriceMap;
	
	static {
		ticketPriceMap = new HashMap<String, Integer>();
		ticketPriceMap.put("1", 1501);
		ticketPriceMap.put("2", 1502);
		ticketPriceMap.put("3", 1503);
		ticketPriceMap.put("4", 1504);
		ticketPriceMap.put("5", 1505);
		ticketPriceMap.put("6", 1506);
		ticketPriceMap.put("7", 1507);
		ticketPriceMap.put("8", 1508);
		ticketPriceMap.put("9", 1509);
	}
	
	protected String readFile(String path) throws IOException, URISyntaxException {
		return readFile(path, StandardCharsets.UTF_8);
	}
	
	protected String readFile(String path, Charset encoding) throws IOException, URISyntaxException {
		byte[] encoded = Files.readAllBytes(Paths.get(AbstractServiceResponder.class.getResource(path).toURI()));
		return new String(encoded, encoding);
	}
	
	protected String changeElementValue(String readFile, Object value, String elementName) {
		return readFile.replaceAll("(<" + elementName + ">)(.*)(<\\/" + elementName + ">)", "$1" + value + "$3");
	}
	
	protected String changeElementValue(String readFile, Object value) {
		return changeElementValue(readFile, value, "Balance");
	}
}
