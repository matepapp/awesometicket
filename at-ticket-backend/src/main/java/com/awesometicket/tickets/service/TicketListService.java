package com.awesometicket.tickets.service;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.xml.source.StringSourceFactory;
import org.w3c.dom.Node;

public class TicketListService extends AbstractServiceResponder {
	
	public Source ticketListResponse(DOMSource request) {
		if (request == null || request.getNode() == null) {
			// TODO: send back proper error
			throw new IllegalArgumentException();
		}
		
		String userId = null;
		if (request.getNode().getChildNodes() != null && request.getNode().getChildNodes().getLength() > 0) {
			Node node = request.getNode().getChildNodes().item(0);
			userId = node.getTextContent();
		}
		
		try {
			return new StringSourceFactory().createSource(readFile(getFileLocation(userId)));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			// TODO: send back error
			throw new IllegalArgumentException();
		}
	}
	
	private String getFileLocation(String userId) {
		String fileName = DEFAULT_TICKET_LIST_FILE_LOCATION;
		
		if ("10".equalsIgnoreCase(userId)) {
			fileName = "/com/awesometicket/response/TicketList_10.xml";
		}
		
		return fileName;
	}
}
