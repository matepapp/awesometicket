package com.awesometicket.integration;

import java.util.Collections;
import java.util.List;

import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.router.AbstractMappingMessageRouter;
import org.springframework.messaging.Message;

public class IntegrationRouter extends AbstractMappingMessageRouter {

	@Override
	protected List<Object> getChannelKeys(Message<?> message) {
		if (message == null || message.getPayload() == null || !(message.getPayload() instanceof DOMSource)) {
			return null;
		}
		
		DOMSource payloadDom = (DOMSource) message.getPayload();
		String nodeName = removeNameSpace(payloadDom.getNode().getNodeName());
		
		return Collections.singletonList(getChannelMappings().get(nodeName));
	}

	private String removeNameSpace(String name) {
		if (name == null || name.isEmpty()) {
			return "";
		}
		
		String[] splittedName = name.split(":");
		if (splittedName == null || splittedName.length < 1) {
			return "";
		}
		
		if (splittedName.length >= 2) {
			return splittedName[1];
		} else {
			return splittedName[0];
		}
	}
}
